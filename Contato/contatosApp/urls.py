from django.urls import path
from .views import *

urlpatterns = [
    path('list/', contact_list, name='contact_list'),
    path('new/', contact_new, name='contact_new'),
    path('update/<int: id>/', contact_update, name='contact_update'),
    path('delete/<int:id>/', contact_delete, name='contact_delete'),


]