from django.db import models

class Contact(models.Model):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    phone = models.IntegerField(max_length=12)
    email = models.EmailField(max_length=60)
    cpf = models.CharField(max_length=25)
    birthday = models.DateTimeField()

    def __str__(self):
        return self.first_name


